#!/usr/bin/env boot

  (set-env!
    :source-paths   #{"development" "sources"}
    :resource-paths #{"resources"})

  (require 'build-utilities)

  (set-env!
   :dependencies (build-utilities/read-dependencies!)
   :exclusions '[org.clojure/clojure org.clojure/clojurescript]
   :main-class 'skim.core)

  (task-options! aot {:namespace #{(get-env :main-class)}}
                 jar {:main (get-env :main-class)}
                 pom {:project 'skim
                      :version "0.1.0"})

  (require '[jeluard.boot-notify :as notify]
           '[pandeiro.boot-http :as bh]
           '[adzerk.boot-reload :as br]
           '[adzerk.boot-cljs :as bc]
           '[development-handler]
           '[clojure.tools.namespace.repl :as nrepl-tools]
           '[deraen.boot-sass :as boot-sass :refer [sass]]
           '[dirac.nrepl]
           )

; === App tasks

  (deftask set-reloadable!
    ""
    []

    (apply nrepl-tools/set-refresh-dirs (get-env :directories))
    identity)

  (deftask development
    "Runs development"
    []

    ; (set-env! :source-paths #(conj % "test"))
    (comp
      (watch)
      (build-utilities/update-dependencies)
      (build-utilities/generate-leiningen-project)
      (bh/serve :handler 'development-handler/my-dir-handler :port 8080)
      (br/reload :on-jsload 'skim.frontend.core/application-reload!)
      (boot-sass/sass)
      (repl :server true
            :port 8230
            :middleware '[dirac.nrepl/middleware])
      (bc/cljs :source-map true
               :source-map-timestamp true
               :optimizations :none
               ; :ids #{"public/assets/javascripts/application"}
               :compiler-options {;:parallel-build true
                                  :compiler-stats true}
                                  :devcards true)
      (set-reloadable!)
      (notify/notify)
      (target :dir #{"target"})))

  (deftask build
    "Builds an uberjar of this project that can be run with java -jar"
    []

    (comp
      (aot)
      (pom)
      (uber)
      (jar)))

  (defn start-dirac! []
    (require 'dirac.agent)
    (let [boot-dirac! (resolve 'dirac.agent/boot!)]
      (boot-dirac!)))

  (start-dirac!)

  (defn start! []
    (require 'skim.backend.core)
    (apply (resolve 'skim.backend.core/start!) []))

  (defn stop! []
    (require 'skim.backend.core)
    (apply (resolve 'skim.backend.core/stop!) []))

  (defn refresh! []
    (stop!)
    (nrepl-tools/refresh))

  (defn refresh-all! []
    (stop!)
    (nrepl-tools/refresh-all))

  (defn reset! []
    (stop!)
    (nrepl-tools/refresh :after 'boot.user/start!))