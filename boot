#!/usr/bin/env bash
(
read -d '' BOOT_JVM_OPTIONS<<-EOF
$JAVA_OPTS -Xmx2g -client -XX:+UseCompressedOops -XX:+TieredCompilation \
-XX:TieredStopAtLevel=1 -Xverify:none -Dclojure.compiler.disable-locals-clearing=true \
-XX:-OmitStackTraceInFastThrow -XX:+CMSClassUnloadingEnabled -XX:+UseG1GC -XX:MaxGCPauseMillis=200 \
-XX:ParallelGCThreads=10 -XX:ConcGCThreads=4 -XX:InitiatingHeapOccupancyPercent=75 \
-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=${ACDC_JDPA_PORT:-5005}
EOF

BOOT_JVM_OPTIONS="$BOOT_JVM_OPTIONS" BOOT_EMIT_TARGET="no" boot "$@";
)
