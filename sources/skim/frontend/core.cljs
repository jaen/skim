(ns skim.frontend.core
  (:require [devtools.core :as devtools]
            [dirac.runtime :as dirac]
            [taoensso.timbre :as log]
            [reagent.core :as reagent]
            ; [re-frame.core :as re-frame]

            [skim.frontend.utilities.logging :as logging-utilities]
            [skim.frontend.application :as application]))

(defn install-devtools! []
  (devtools/set-pref! :install-sanity-hints true)
  (devtools/install!)
  (dirac/install!))

(defn mount-root! []
  (reagent/render [#'application/application-component]
                  (. js/document (querySelector "#application"))))

(defn application-initialise! []
  (install-devtools!)
  (when-let [appender (logging-utilities/make-console-appender)]
    (log/merge-config! {:appenders {:console appender}}))
  (log/debug "Starting the application.")
  (mount-root!))

(defn application-reload! []
  (log/debug "Reloading the application.")
  (mount-root!))
