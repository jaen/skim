(ns skim.backend.components.application
  (:require [mount.core :as mount]

            [skim.backend.components.server]))

(mount/defstate application
  :start (mount/start-with-args (mount/args)
           #'skim.backend.components.server/server)
  :stop (mount/stop
          #'skim.backend.components.server/server))
