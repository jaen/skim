(ns skim.backend.components.server
  (:require [aleph.http :as http]
            [mount.core :as mount]
            [taoensso.timbre :as log]
            [ring.util.http-response :refer [ok not-found]]
            [ring.middleware.keyword-params :refer [wrap-keyword-params]]
            [ring.middleware.params :refer [wrap-params]]
            [ring.middleware.stacktrace :as ring-stacktrace]
            [ring.middleware.format :refer [wrap-restful-format]]
            ; [ring.middleware.cors :refer [wrap-cors]]

            [skim.common.utilities.transit :as transit]
            #_[skim.backend.api.core :as api]))
;; ===

  (def all-cors-headers
    {"Access-Control-Allow-Origin"  "*"
     "Access-Control-Allow-Headers" "*"
     "Access-Control-Allow-Methods" "*"})

  (defn all-cors
    "Allow requests from all origins"
    [handler]
    (fn [request]
      (let [response (handler request)]
        (update-in response [:headers]
          merge all-cors-headers))))

  (defn- build-ring-handler [] ; routes handlers]
    (-> (fn [req] {:status 200 :headers {"ContentType" "text/plain"} :body "test"}) ;() (bidi-ring/make-handler routes handlers)
      (wrap-restful-format :formats [:transit-json]
        :response-options {:transit-json {:handlers transit/write-handlers}}
        :params-options {:transit-json {:handlers  transit/read-handlers}})
      (wrap-keyword-params)
      (wrap-params)
      (all-cors)
      #_(wrap-cors :access-control-allow-origin [#".*"]
          :access-control-allow-methods [:get :put :post :delete])))

  (defn- wrap-development [handler]
    (ring-stacktrace/wrap-stacktrace handler))

  (def wrapped-handler
    (-> (build-ring-handler) ; api/routes api/handlers)
      wrap-development))

;; === Application initialisation ===

  (defn start!
    "Starts the application."
    [config]
    (let [{:keys [environment port]} config
          server-params              (when (= environment :development)
                                       {:host "0.0.0.0"
                                        :port port})]
      (log/info "Starting the application with: " {:environment environment
                                                   :port        port})
      ; (migrations/ensure-database!)
      (condp = environment
        :production (http/start-server wrapped-handler (or server-params {}))
        (http/start-server #'wrapped-handler server-params))))

  (defn stop!
    "Stops the application."
    [server-handle]

    (when (.close server-handle)
      nil))

(mount/defstate server
  :start (start! (mount/args))
  :stop (stop! server))