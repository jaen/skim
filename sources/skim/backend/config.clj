(ns skim.backend.config
  (:refer-clojure :exclude [get])
  (:require [environ.core :as environ]))

;;

  (defn- get-config-value [key default & [cast-fn]]
    (let [cast-fn (or cast-fn identity)]
      (or (some-> environ/env key cast-fn)
        default)))

;; Return configuration

  (defn integer [val]
    (Integer. val))

  (defn get []
    (let [environment     (get-config-value :skim-env            :development  keyword)
          host            (get-config-value :skim-host           "0.0.0.0")
          port            (get-config-value :skim-port           4242          integer)]
      {:environment environment
       :host host
       :port port}))
